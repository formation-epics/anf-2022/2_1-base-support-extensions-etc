# 2_1 base support extensions etc

> 30 mn

Cette partie prendra plutôt la forme d'une démonstration, et ce document servira de support.
N'importe qui devrait pouvoir reproduire la démonstration à partir de ce document.

## Table des matières

<!-- vim-markdown-toc GitLab -->

* [Prérequis](#prérequis)
* [Références](#références)
* [Installer EPICS from scratch](#installer-epics-from-scratch)
    * [Création du groupe epics](#création-du-groupe-epics)
    * [Gestion des dépendances](#gestion-des-dépendances)
        * [Astuce OPTIONNELLE pour aller plus loin](#astuce-optionnelle-pour-aller-plus-loin)
    * [Création de `/opt/epics`](#création-de-optepics)
    * [Installation de la base EPICS](#installation-de-la-base-epics)
    * [Installation du support EPICS](#installation-du-support-epics)
    * [Installation d'extensions EPICS](#installation-dextensions-epics)
    * [Configuration de vos variables d'environnement](#configuration-de-vos-variables-denvironnement)
        * [Astuce OPTIONNELLE pour aller plus loin](#astuce-optionnelle-pour-aller-plus-loin-1)
* [Créer et utiliser un top EPICS](#créer-et-utiliser-un-top-epics)
    * [Utiliser sa propre base de données](#utiliser-sa-propre-base-de-données)
    * [Démarrer son programme IOC](#démarrer-son-programme-ioc)
    * [Ajouter des modules à son top EPICS](#ajouter-des-modules-à-son-top-epics)
    * [IOC Shell](#ioc-shell)

<!-- vim-markdown-toc -->

## Prérequis

- [Rocky Linux 8](https://rockylinux.org/)

## Références

- <https://epics-controls.org>
- <https://docs.epics-controls.org>
- <https://docs.epics-controls.org/projects/how-tos/en/latest/getting-started/installation.html>
- <https://docs.epics-controls.org/en/latest/appdevguide/gettingStarted.html?highlight=Shell#chap:IOCShell>
- <https://epics.anl.gov/index.php>
- <https://epics.anl.gov/base/index.php>
- <https://epics.anl.gov/modules/index.php>
- <https://epics.anl.gov/extensions/index.php>
- <https://epics.anl.gov/download/index.php>
- <https://epics.anl.gov/tech-talk/index.php>
- <https://epics.anl.gov/docs/training.php>

## Installer EPICS from scratch

Cette méthode d'installation est la même, quelle que soit la version d'EPICS (`3.15` ou `7.0`).
Ici, pour l'exemple, la version `7.0` sera installée (la dernière version stable au moment d'écrire
ce guide).

### Création du groupe epics

Je considère que l'utilisateur courant sera celui qui va installer EPICS et qui va l'utiliser.
Donc, connectez-vous sur votre VM Rocky Linux avec l'utilisateur `epicslearner` (l'utilisateur par
défaut sur votre VM, vous pouvez le vérifier en exécutant la commande `$ whoami` dans un terminal).

Pour commencer, il faut créer un groupe d'utilisateurs `epics` sur votre machine, et il faut y
ajouter l'utilisateur courant :
```console
$ sudo groupadd epics
$ sudo usermod -aG epics $USER
$ newgrp epics
```

Vous pouvez vérifier que votre utilisateur fait bien partie du nouveau groupe `epics` avec la
commande suivante :
```console
$ groups
```

### Gestion des dépendances

- Le projet [Core DNF Plugins
  associé](https://github.com/rpm-software-management/dnf-plugins-core/)) permet d'augmenter DNF
  avec plusieurs nouvelles commandes pratiques. Installez-le ainsi :

    ```console
    $ sudo dnf install dnf-plugins-core
    ```

- EPEL ("Extra Packages for Enterprise Linux") est un répertoire de paquets (package repository) de
  qualités, contenant beaucoups de paquets ne se trouvant dans les répertoires par défaut. Il est
  nécessaire d'installer ce répertoire pour la suite :

    ```console
    $ sudo dnf install epel-release
    $ sudo dnf repolist | grep epel && echo OK
    ```

- PowerTools est un autre répertoire de paquets orienté pour les déleveppeurs (ce répertoire contient surtout des librairies et des outils de développement). Il est souvent recommandé de l'installer en complément du répertaoire EPEL :

    ```console
    $ sudo dnf config-manager --set-enabled powertools
    $ sudo dnf repolist | grep powertools && echo OK
    ```

- A présent, vous pouvez installer les dépendances nécessaires à l'installation et à l'utilisation
  d'EPICS. C'est-à-dire les paquets suivants  :

    ```console
    $ sudo dnf install wget                                                         # needed to retrieve the EPICS base, support, etc
    $ sudo dnf install make perl gcc gcc-c++                                        # EPICS base dependencies
    $ sudo dnf install re2c                                                         # SEQ dependency (for SNL sequencing)
    $ sudo dnf install rpcsvc-proto-devel rpcgen libtirpc-devel                     # Asyn dependencies
    $ sudo dnf install xorg-x11-proto-devel libX11-devel libXext-devel libusb-devel # Area Detector dependencies
    ```

#### Astuce OPTIONNELLE pour aller plus loin

Pour d'autres distributions GNU/Linux, vous pouvez utiliser [repology.org](https://repology.org/)
et/ou [pkgs.org](https://pkgs.org/) pour trouver les dépendances équivalentes.

### Création de `/opt/epics`

Le dossier `/opt/epics` est l'emplacement qui accueillera la base EPICS (ainsi que le support) :
```console
$ sudo mkdir /opt/epics
$ sudo chown root:epics /opt/epics && sudo chmod 775 /opt/epics
$ sudo chmod g+s /opt/epics # set setgid bit for files/dirs under /opt/epics to inherit group rights
```

### Installation de la base EPICS

Ici nous allons télécharger et compiler la dernière version de la base `7.0` pour l'exemple, mais
la version `3.15` pourrait être installée de la même manière :
```console
$ cd /opt/epics
$ git clone --recursive -b 7.0 https://git.launchpad.net/epics-base base-7.0
$ ln -s base-7.0 base
$ cd base
$ make clean && make && echo OK || echo KO # cela peut prendre plus ou moins de temps selon votre machine
```

### Installation du support EPICS

Le support EPICS est un dossier contenant des "modules". Un module n'est rien d'autre qu'un top
EPICS développé par un tiers qui pourra vous faciliter le développement de vos IOCs. Pour rappel,
un top est juste un dossier respectant une certaine architecture spécifique à EPICS.

Par exemple, si vous souhaitez développer un IOC lié à une caméra, alors vous aurez de bonnes
chances de trouver ce qu'il vous faut avec le module
[areaDetector](https://github.com/areaDetector/areaDetector). Vous évitant ainsi d'avoir à
développer un driver EPICS from scratch (alors qu'un membre de la communauté EPICS l'aurait déjà
fait).

Vous pouvez vous créer un support à la main, mais pour faciliter les choses dans le cadre de cette
démonstration je vous propose d'utiliser [synApps](https://github.com/EPICS-synApps/support).
synApps est une collection réputée de modules EPICS maintenu par un membre "dévoué" de la
communauté EPICS (Tim Mooney).

- Téléchargez et configurez [synApps R6-2-1](https://github.com/EPICS-synApps/support/tree/R6-2-1) :
    ```console
    $ cd /opt/epics
    $ wget https://raw.githubusercontent.com/EPICS-synApps/support/R6-2-1/assemble_synApps.sh

    $ echo "set EPICS base location"
    $ sed -i 's/^EPICS_BASE=.*/EPICS_BASE=\/opt\/epics\/base/g' assemble_synApps.sh # set EPICS base location

    $ echo "use a more recent version release of [camac](https://github.com/epics-modules/camac)"
    $ echo "see https://github.com/epics-modules/camac/commit/5dea4210bef24a12a85ad8cc3d0c85c7772c2584"
    $ sed -i 's/^CAMAC=R2-7-3/CAMAC=R2-7-4/g' assemble_synApps.sh

    $ echo "use a more recent version release of [sscan](https://github.com/epics-modules/sscan)"
    $ echo "see https://github.com/epics-modules/sscan/issues/20"
    $ echo "see https://github.com/epics-modules/sscan/commit/420274ca2e4331e92119bd0524d0bcd7ffdd9f93"
    $ sed -i 's/^SSCAN=R2-11-4/SSCAN=R2-11-5/g' assemble_synApps.sh

    $ echo "use a more recent version release of [caputrecorder](https://github.com/epics-modules/caputRecorder)"
    $ echo "see https://github.com/epics-modules/caputRecorder/issues/4"
    $ echo "see https://github.com/epics-modules/caputRecorder/pull/5"
    $ sed -i 's/^CAPUTRECORDER=R1-7-3/CAPUTRECORDER=R1-7-4/g' assemble_synApps.sh

    $ bash ./assemble_synApps.sh
    $ mv synApps synApps-6.2.1
    $ ln -s synApps-6.2.1 synApps
    $ ln -s synApps/support support
    ```

- Paramètrez le module [Asyn R4.42](https://github.com/epics-modules/asyn/tree/R4-42) :
    ```console
    $ echo "set TIRPC=YES in synApps [Asyn module](https://github.com/epics-modules/asyn/tree/R4-42)"
    $ cd /opt/epics/synApps/support/asyn-R4-42
    $ sed -i 's/^# TIRPC=.*/TIRPC=YES/g' configure/CONFIG_SITE
    ```

- Appliquez un petit patch ([`./patches/784e419.patch`](./patches/784e419.patch), qui se trouve
  dans le dossier `patches` à côté de ce README) :
    ```console
    $ echo "fix [motor R7-2-2](https://github.com/epics-modules/motor/tree/R7-2-2)"
    $ echo "see https://epics.anl.gov/tech-talk/2021/msg01911.php"
    $ echo "see https://github.com/epics-modules/motor/commit/784e41927b942d38c31a766309668d6114861b9e"
    $ cd /opt/epics/synApps/support/motor-R7-2-2
    $ if ! $(grep -q "#include <shareLib.h>" motorApp/MotorSrc/motordrvCom.h); then git apply /path/to/2_1-base-support-extensions-etc/patches784e419.patch; fi
    ```
    synApps est un projet ambitieux, mais essentiellement maintenu par une seule personne. Il n'est
    donc pas raisonnable d'attendre de Tim Mooney de pouvoir supporter tout type de distribution,
    d'architecture CPU, etc. Occasionnellement, il est donc parfois nécessaire d'appliquer quelques
    petits patch à certains modules (ici il suffisait juste d'ajouter `#include <shareLib.h>` à un
    `.h`)

- Préparez la compilation en exportant quelques variables d'environnement nécessaires à EPICS (plus
  de détails à ce sujet à venir) :
    ```console
    $ export EPICS_BASE=/opt/epics/base
    $ export EPICS_HOST_ARCH=$(${EPICS_BASE}/startup/EpicsHostArch)
    $ export PATH=${EPICS_BASE}/bin/${EPICS_HOST_ARCH}:${PATH}
    $ export LD_LIBRARY_PATH=${EPICS_BASE}/lib/${EPICS_HOST_ARCH}:${LD_LIBRARY_PATH}
    ```

- Compilez le tout :
    ```console
    $ cd /opt/epics/synApps/support
    $ make release
    $ make clean && make rebuild && echo OK || echo KO # cela peut prendre plus ou moins de temps selon votre machine
    ```

### Installation d'extensions EPICS

Les extensions EPICS sont tous les outils tiers qui ne sont pas considérés comme des modules. Cela
peut aller d'une library Python pour échanger sur le protocole de communication EPICS, à un plugin
WireShark, en passant par un logiciel permettant de visualiser l'architecture de ses bases de
données EPICS.

Une extension EPICS très connue est [Phoebus](https://controlssoftware.sns.ornl.gov/css_phoebus/) :
un logiciel permettant d'éditer et de lire des IHM (Interface Homme-Machine). Donc un outil
graphique qui permet d'interagir avec ses IOCs.

Chacun peut utiliser les extensions qu'il souhaite, voici par exemple comment simplement installer
la dernière version de Phoebus :
```console
$ cd /opt/epics
$ mkdir extensions && cd extensions
$ wget https://controlssoftware.sns.ornl.gov/css_phoebus/nightly/phoebus-linux.zip
$ unzip phoebus-linux.zip
$ rm phoebus-linux.zip
```

### Configuration de vos variables d'environnement

Plusieurs variables d'environnements sont nécessaires pour que vous puissiez utiliser EPICS.

Éditez votre `/etc/bashrc` (ou `$HOME/.bashrc`) (en exécutant par exemple `$ sudo gedit
/etc/bashrc`, ou `$ sudo nano /etc/bashrc`, ou `$ sudo vi /etc/bashrc`, ou peu importe...) et
ajoutez-y le contenu suivant à la fin :
```console
...

# EPICS base
export EPICS_BASE=/opt/epics/base
export EPICS_HOST_ARCH=$(${EPICS_BASE}/startup/EpicsHostArch)
export PATH=${EPICS_BASE}/bin/${EPICS_HOST_ARCH}:${PATH}
export LD_LIBRARY_PATH=${EPICS_BASE}/lib/${EPICS_HOST_ARCH}:${LD_LIBRARY_PATH}

# EPICS support
export EPICS_SUPPORT=/opt/epics/support
export PATH=${EPICS_SUPPORT}/bin/${EPICS_HOST_ARCH}:${PATH}
export LD_LIBRARY_PATH=${EPICS_SUPPORT}/lib/${EPICS_HOST_ARCH}:${LD_LIBRARY_PATH}

# EPICS extensions
alias phoebus="cd /tmp && /opt/epics/extensions/phoebus-4.6.6/phoebus.sh"

# EPICS config
export EPICS_CA_AUTO_ADDR_LIST=no
export EPICS_CA_ADDR_LIST="127.0.0.1"
```

N'oubliez pas de sourcer ce fichier dans tous vos terminaux ouverts avec la commande `$ source
/etc/bashrc`(ou plus simplement, fermez/redémarrez tous vos terminaux).

#### Astuce OPTIONNELLE pour aller plus loin

Plutôt que d'utiliser un fichier de configuration global à tout votre système (`/etc/bashrc`) ou à
tout votre utilisateur (`$HOME/.bashrc`), vous préféreriez probablement pouvoir configurer vos
variables d'environnement par projet/repository.

Le projet [direnv](https://github.com/direnv/direnv) permet exactement de faire cela. Vous pouvez
l'installer en suivant [cette documentation](https://direnv.net/docs/installation.html).

Une fois installé, vous pouvez vous diriger vers votre projet (par exemple votre top) et y créer le
`.envrc` associé :
```console
$ cd /path/to/repository
$ vi .envrc
    > # EPICS base
    > export EPICS_BASE=/opt/epics/base
    > #export EPICS_HOST_ARCH=$(${EPICS_BASE}/startup/EpicsHostArch)
    > export EPICS_HOST_ARCH=linux-x86_64
    > export PATH=${EPICS_BASE}/bin/${EPICS_HOST_ARCH}:${PATH}
    > export LD_LIBRARY_PATH=${EPICS_BASE}/lib/${EPICS_HOST_ARCH}:${LD_LIBRARY_PATH}
    >
    > # EPICS support
    > export EPICS_SUPPORT=/opt/epics/support
    > export PATH=${EPICS_SUPPORT}/bin/${EPICS_HOST_ARCH}:${PATH}
    > export LD_LIBRARY_PATH=${EPICS_SUPPORT}/lib/${EPICS_HOST_ARCH}:${LD_LIBRARY_PATH}
    >
    > # EPICS extensions
    > alias phoebus="cd /tmp && /opt/epics/extensions/phoebus-4.6.6/phoebus.sh"
    >
    > # EPICS config
    > export EPICS_CA_AUTO_ADDR_LIST=yes
    > export EPICS_CA_ADDR_LIST=""

$ direnv allow
```

L'intérêt de pouvoir spécifier des variables d'environnement différentes par projet est pratique
sur un PC de développement. En effet, les différents projets EPICS sur lesquels vous travaillez
peuvent utiliser des modules à des versions différentes, voir une base EPICS à des versions
différentes (`3.15` et `7.0` typiquement). Il est donc bien utile de pouvoir spécifier ces
différences par projet.

## Créer et utiliser un top EPICS

- Voici comment créer un top composé de deux applications (pour rappel une application, ou App,
  EPICS est une brique logique - dans un top - comprenant une base de données et le code associé
  lui permettant de remplir sa fonction) :
    ```console
    $ mkdir -p $HOME/tops/tip-top
    $ cd $HOME/tops/tip-top
    $ makeBaseApp.pl -a linux-x86_64 -t example fooname && makeBaseApp.pl -a linux-x86_64 -i -t example -p fooname fooname
    $ makeBaseApp.pl -a linux-x86_64 -t ioc barname && makeBaseApp.pl -a linux-x86_64 -i -t ioc -p barname barname
    ```

    Remarque :
    - La commande `makeBaseApp.pl` va permettre de générer l'architecture d'un top, ainsi que les
      applications (ou "app") qui le compose, et les dossiers permettant de "booter"/démarrer ses
      programmes IOC.
    - `makeBaseApp.pl -a linux-x86_64 -t example fooname` permet de créer l'application "fooname"
      de type "example" (type utile pour découvrir, jouer et tester le fonctionnement d'une app
      EPICS).
    - `makeBaseApp.pl -a linux-x86_64 -t ioc barname` permet de créer l'application "barname" de
      type "ioc" (type utile comme template de base pour une app qui ira en production).
    - `makeBaseApp.pl -l` permet de lister les différents types d'applications valides.
    - `makeBaseApp.pl -h` permet de rapidement consulter comment utiliser `makeBaseApp.pl`.
    - `makeBaseApp.pl -a linux-x86_64 -i -t example -p fooname fooname` permet de créer l'entrée
      correspondant à `fooname` dans le dossier `iocBoot` d'où le programme IOC pourra
      "booter"/démarrer.
    - `makeBaseApp.pl -a linux-x86_64 -i -t ioc -p barname barname` permet de créer l'entrée
      correspondant à `barname` dans le dossier `iocBoot` d'où le programme IOC pourra
      "booter"/démarrer.

- Voici l'architecture générée par les précédentes commandes :
    ```console
    $ tree
    .
    ├── barnameApp
    │   ├── Db
    │   │   └── Makefile
    │   ├── Makefile
    │   └── src
    │       ├── barnameMain.cpp
    │       └── Makefile
    ├── configure
    │   ├── CONFIG
    │   ├── CONFIG_SITE
    │   ├── Makefile
    │   ├── RELEASE
    │   ├── RULES
    │   ├── RULES_DIRS
    │   ├── RULES.ioc
    │   └── RULES_TOP
    ├── foonameApp
    │   ├── Db
    │   │   ├── circle.db
    │   │   ├── dbExample1.db
    │   │   ├── dbExample2.db
    │   │   ├── dbSubExample.db
    │   │   ├── foonameVersion.db
    │   │   ├── Makefile
    │   │   └── user.substitutions
    │   ├── Makefile
    │   └── src
    │       ├── dbSubExample.c
    │       ├── dbSubExample.dbd
    │       ├── devfoonameVersion.c
    │       ├── devfoonameVersion.dbd
    │       ├── devXxxSoft.c
    │       ├── foonameHello.c
    │       ├── foonameHello.dbd
    │       ├── foonameMain.cpp
    │       ├── initTrace.c
    │       ├── initTrace.dbd
    │       ├── Makefile
    │       ├── sncExample.dbd
    │       ├── sncExample.stt
    │       ├── sncProgram.st
    │       ├── xxxRecord.c
    │       ├── xxxRecord.dbd
    │       └── xxxSupport.dbd
    ├── iocBoot
    │   ├── iocbarname
    │   │   ├── Makefile
    │   │   └── st.cmd
    │   ├── iocfooname
    │   │   ├── Makefile
    │   │   ├── README
    │   │   └── st.cmd
    │   └── Makefile
    └── Makefile
    ```

    Remarque :
    - Beaucoup de dossiers sont apparus dans le dossier `tip-top` qui est à présent devenu un "top"
      EPICS (un top n'est rien d'autre qu'un dossier respectant une certaine architecture dans
      lequel on peut retrouver une ou plusieurs applications) :
        - Le dossier `foonameApp`, il contient tous les fichiers liés à l'application `fooname`, en
          particulier deux sous-dossiers important :
            - Le sous-dossier `Db`, qui contient tous les fichiers `.db` et/ou `.template`
              définissant la base de donnée EPICS.
            - Le sous-dossier `src`, qui contient tout le code source associé à l'application.
        - Le dossier `barameApp` possède globalement la même architecture que `fooname`, on
          remarquera que son dossier `Db` et `src` sont moins fournis que `fooname` car ce dernier
          a été créé à partir du type `example` et fournit donc beaucoup plus de contenu à titre
          d'exemple.
        - Le dossier `iocBoot`, il contient de quoi faire "booter"/démarrer toutes les applications
          associées (par exemple dans `iocBoot`, le sous-dossier `iocfooname` contiendra tous les
          fichiers permettant de lancer `foonameApp`). J'aborderai plus en détails le contenu de ce
          dossier un peu plus loin dans cette démo.
        - Le dossier `configure`, il contient les fichiers nécessaires par EPICS pour builder un
          top (en particulier le fichier `RELEASE` sur lequel je reviendrai plus tard).

- Voici le contenu du Makefile à la racine du top (expliquant pourquoi on appelle un top un top,
  cf. le premier commentaire et la première variable déclarée) :
    ```console
    $ cat Makefile
        > # Makefile at top of application tree
        > TOP = .
        > include $(TOP)/configure/CONFIG
        >
        > # Directories to build, any order
        > DIRS += configure
        > DIRS += $(wildcard *Sup)
        > DIRS += $(wildcard *App)
        > DIRS += $(wildcard *Top)
        > DIRS += $(wildcard iocBoot)
        >
        > # The build order is controlled by these dependency rules:
        >
        > # All dirs except configure depend on configure
        > $(foreach dir, $(filter-out configure, $(DIRS)), \
        >     $(eval $(dir)_DEPEND_DIRS += configure))
        >
        > # Any *App dirs depend on all *Sup dirs
        > $(foreach dir, $(filter %App, $(DIRS)), \
        >     $(eval $(dir)_DEPEND_DIRS += $(filter %Sup, $(DIRS))))
        >
        > # Any *Top dirs depend on all *Sup and *App dirs
        > $(foreach dir, $(filter %Top, $(DIRS)), \
        >     $(eval $(dir)_DEPEND_DIRS += $(filter %Sup %App, $(DIRS))))
        >
        > # iocBoot depends on all *App dirs
        > iocBoot_DEPEND_DIRS += $(filter %App,$(DIRS))
        >
        > # Add any additional dependency rules here:
        >
        > include $(TOP)/configure/RULES_TOP
    ```

- À présent, vous pouvez compiler votre top :
    ```console
    $ make clean && make && echo OK || echo KO
    ```

- Voici à quoi ressemble votre top à présent :
    ```console
    $ tree
    .
    ├── barnameApp
    │   ├── Db
    │   │   ├── Makefile
    │   │   ├── O.Common
    │   │   └── O.linux-x86_64
    │   │       └── ...
    │   ├── Makefile
    │   └── src
    │       ├── barnameMain.cpp
    │       ├── Makefile
    │       ├── O.Common
    │       │   └── ...
    │       └── O.linux-x86_64
    │           └── ...
    ├── bin
    │   └── linux-x86_64
    │       ├── barname
    │       └── fooname
    ├── configure
    │   ├── CONFIG
    │   ├── CONFIG_SITE
    │   ├── Makefile
    │   ├── O.Common
    │   ├── O.linux-x86_64
    │   │   └── ...
    │   ├── RELEASE
    │   ├── RULES
    │   ├── RULES_DIRS
    │   ├── RULES.ioc
    │   └── RULES_TOP
    ├── db
    │   ├── circle.db
    │   ├── dbExample1.db
    │   ├── dbExample2.db
    │   ├── dbSubExample.db
    │   ├── foonameVersion.db
    │   └── user.substitutions
    ├── dbd
    │   ├── barname.dbd
    │   ├── fooname.dbd
    │   ├── xxxRecord.dbd
    │   └── xxxSupport.dbd
    ├── foonameApp
    │   ├── Db
    │   │   ├── circle.db
    │   │   ├── dbExample1.db
    │   │   ├── dbExample2.db
    │   │   ├── dbSubExample.db
    │   │   ├── foonameVersion.db
    │   │   ├── Makefile
    │   │   ├── O.Common
    │   │   ├── O.linux-x86_64
    │   │   │   └── ...
    │   │   └── user.substitutions
    │   ├── Makefile
    │   └── src
    │       ├── dbSubExample.c
    │       ├── dbSubExample.dbd
    │       ├── devfoonameVersion.c
    │       ├── devfoonameVersion.dbd
    │       ├── devXxxSoft.c
    │       ├── foonameHello.c
    │       ├── foonameHello.dbd
    │       ├── foonameMain.cpp
    │       ├── initTrace.c
    │       ├── initTrace.dbd
    │       ├── Makefile
    │       ├── O.Common
    │       │   └── ...
    │       ├── O.linux-x86_64
    │       │   └── ...
    │       ├── sncExample.dbd
    │       ├── sncExample.stt
    │       ├── sncProgram.st
    │       ├── xxxRecord.c
    │       ├── xxxRecord.dbd
    │       └── xxxSupport.dbd
    ├── include
    │   └── xxxRecord.h
    ├── iocBoot
    │   ├── iocbarname
    │   │   ├── envPaths
    │   │   ├── Makefile
    │   │   └── st.cmd
    │   ├── iocfooname
    │   │   ├── envPaths
    │   │   ├── Makefile
    │   │   ├── README
    │   │   └── st.cmd
    │   └── Makefile
    ├── lib
    │   └── linux-x86_64
    │       ├── libfoonameSupport.a
    │       └── libfoonameSupport.so
    └── Makefile
    ```

    Remarque :
    - Beaucoup de produits de compilation ont été ajoutés (contenu dans tous les dossiers
      `O.Common` et `O.linux-x86_64`).
    - Beaucoup de dossiers sont apparus à la racine de votre top (en théorie "la racine d'un top"
      est un pléonasme mais en pratique tout va bien 🙂):
        - Le dossier `bin`, il contient les binaires associés à vos applications (nous verrons plus
          tard comment les utiliser pour démarrer vos programmes IOC).
        - Le dossier `db`, il contient tous les fichiers `.db` utilisés parmi toutes les
          applications de votre top.
            - Les fichiers `.db` (data base) décrivent l'architecture de votre base de donnée
              EPICS, c'est ici que sont définis vos records qui seront ensuite instanciés en PVs.
        - Le dossier `dbd`, il contient tous les fichiers `.dbd` utilisés parmi toutes les
          applications de votre top.
            - Les fichiers `.dbd` (data base definition) sont utilisés pour générer des fichiers
              hearder, utilisés à leur tour pour compiler les fichiers `.db`. Il permettent de
              configurer des éléments liés aux records (il est par exemple possible de modifier
              certains champs, typiquement le champ SCAN peut être personnalisé pour ajouter des
              temps de SCAN).
        - Le dossier `include`, il contient les fichiers `.h` qui ont été auto-générés à partir
          d'éventuels `.dbd` que vous auriez créés parmi toutes les applications de votre top.
        - Le dossier `lib`, il contient les librairies auto-générés par votre top. Généralement un
          `.a` (lib statique) et un `.so` (lib dynamique) par application, par exemple pour que
          votre top puisse être utilisé par un autre top (fonctionnement typique des modules du
          support).
    - Le fichier `envPath` dans les dossiers `./iocBoot/iocbarname` et `./iocBoot/iocfooname`. Ce
      fichier contient des variables d'environnement auto-générées par EPICS qui seront chargées au
      moment de lancer vos programmes IOC.

### Utiliser sa propre base de données

L'objectif ici est simplement de savoir comment créer sa propre base de données et l'inclure à son
IOC.

Créez le fichier `tip.db` (un fichier data base), juste histoire d'y définir un record :
```console
$ vi $HOME/tops/tip-top/fooname/Db/tip.db

    > record(ao, "tiptop:fooname:foo") {
    >   field(VAL , "42")
    > }
```

N'oubliez pas de spécifier ce fichier dans le Makefile associé :
```console
$ vi $HOME/tops/tip-top/fooname/Db/Makefile
    > TOP=../..
    > include $(TOP)/configure/CONFIG
    > #----------------------------------------
    > #  ADD MACRO DEFINITIONS BELOW HERE
    >
    > # Install databases, templates & substitutions like this
    > DB += circle.db
    > DB += dbExample1.db
    > DB += dbExample2.db
    > DB += foonameVersion.db
    > DB += dbSubExample.db
    > DB += user.substitutions
    >
    > DB += tip.db
    >
    > # If <anyname>.db template is not named <anyname>*.template add
    > # <anyname>_TEMPLATE = <templatename>
    >
    > include $(TOP)/configure/RULES
    > #----------------------------------------
    > #  ADD EXTRA GNUMAKE RULES BELOW HERE
```

Et enfin n'oubliez pas de recompiler votre top :
```console
$ make clean && make && echo OK || echo KO
```

Il faudra enfin penser à charger ce nouveau `.db` au moment de lancer son programme IOC, comme
expliqué dans la section suivante.

### Démarrer son programme IOC

Pour lancer son programme IOC, tout se joue dans l'entrée `iocBoot` correspondante. Par exemple
pour lancer l'IOC associé à `foonameApp`, alors il faudra aller voir dans `./iocBoot/iocfooname` :
```console
$ cd $HOME/tops/tip-top/iocBoot/iocfooname/

$ cat envPath
    > epicsEnvSet("IOC","iocfooname")
    > epicsEnvSet("TOP","/home/epicslearner/Tops/tip-top")
    > epicsEnvSet("EPICS_BASE","/opt/epics/base-7.0.6.1")

$ cat st.cmd
    > #!../../bin/linux-x86_64/fooname
    >
    > #- You may have to change fooname to something else
    > #- everywhere it appears in this file
    >
    > < envPaths
    >
    > cd "${TOP}"
    >
    > ## Register all support components
    > dbLoadDatabase "dbd/fooname.dbd"
    > fooname_registerRecordDeviceDriver pdbbase
    >
    > ## Load record instances
    > dbLoadTemplate "db/user.substitutions"
    > dbLoadRecords "db/foonameVersion.db", "user=epicslearner"
    > dbLoadRecords "db/dbSubExample.db", "user=epicslearner"
    >
    > #- Set this to see messages from mySub
    > #var mySubDebug 1
    >
    > #- Run this to trace the stages of iocInit
    > #traceIocInit
    >
    > cd "${TOP}/iocBoot/${IOC}"
    > iocInit
    >
    > ## Start any sequence programs
    > #seq sncExample, "user=epicslearner"
```

Remarques :
- `#!../../bin/linux-x86_64/fooname` : si le fichier est exécutable alors il va chercher le binaire
  à cet emplacement.
- `< envPaths` : charge les variables d'environnements générées.
- `cd "${TOP}"` : change de dossier à la racine du top (pour se placer à côté du dossier `db` et
  `dbd`).
- toutes les commandes commençant par `dbLoad...` permettent de charger les différents `.db`,
  `.dbd`, `.template`, `.substitutions`, etc.
- `cd "${TOP}/iocBoot/${IOC}"` : change de dossier pour se retrouver à côté du `.cmd`.
- `iocInit` : démarre le programme IOC.

Si vous avez créé de nouveaux `.db`, alors n'oubliez pas de les charger dans le `.cmd`, par exemple
`tip.db` (cf. section précédente) serait chargé de cette façon :
```console
$ vi st.cmd

    > ...
    > ## Load record instances
    > dbLoadRecords "db/tip.db"
    > ...
```

Pour démarrer son programme IOC, il suffit de rendre son `.cmd` exécutable et de le lancer
(alternativement on peut aussi directement utiliser le fichier binaire généré et lui passer le
`.cmd` en argument) :
```console
$ chmod +x st.cmd
$ ./st.cmd # or `../../bin/linux-x86_64/fooname st.cmd`
```

### Ajouter des modules à son top EPICS

Par exemple en ajoutant le module SEQ ("SNCSEQ") permettant de faire du SNL :
```console
# RELEASE - Location of external support modules
#
# IF YOU CHANGE ANY PATHS in this file or make API changes to
# any modules it refers to, you should do a "make rebuild" in
# this application's top level directory.
#
# The EPICS build process does not check dependencies against
# any files from outside the application, so it is safest to
# rebuild it completely if any modules it depends on change.
#
# Host- or target-specific settings can be given in files named
#  RELEASE.$(EPICS_HOST_ARCH).Common
#  RELEASE.Common.$(T_A)
#  RELEASE.$(EPICS_HOST_ARCH).$(T_A)
#
# This file is parsed by both GNUmake and an EPICS Perl script,
# so it may ONLY contain definititions of paths to other support
# modules, variable definitions that are used in module paths,
# and include statements that pull in other RELEASE files.
# Variables may be used before their values have been set.
# Build variables that are NOT used in paths should be set in
# the CONFIG_SITE file.

# Variables and paths to dependent modules:
#MODULES = /path/to/modules
#MYMODULE = $(MODULES)/my-module

#MODULES = /opt/epics/support
SUPPORT = /opt/epics/support

# If using the sequencer, point SNCSEQ at its top directory:
#SNCSEQ = $(MODULES)/seq-2-2-9
SNCSEQ = $(SUPPORT)/seq-2-2-9

# EPICS_BASE should appear last so earlier modules can override stuff:
EPICS_BASE = /opt/epics/base-7.0.6.1

# Set RULES here if you want to use build rules from somewhere
# other than EPICS_BASE:
#RULES = $(MODULES)/build-rules

# These lines allow developers to override these RELEASE settings
# without having to modify this file directly.
-include $(TOP)/../RELEASE.local
-include $(TOP)/../RELEASE.$(EPICS_HOST_ARCH).local
-include $(TOP)/configure/RELEASE.local
```

Puis en dé-commentant la dernière ligne du `st.cmd` :
```console
#!../../bin/linux-x86_64/fooname

#- You may have to change fooname to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/fooname.dbd"
fooname_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadTemplate "db/user.substitutions"
dbLoadRecords "db/foonameVersion.db", "user=epicslearner"
dbLoadRecords "db/dbSubExample.db", "user=epicslearner"

#- Set this to see messages from mySub
#var mySubDebug 1

#- Run this to trace the stages of iocInit
#traceIocInit

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
seq sncExample, "user=epicslearner"
```

Plus de détails sur le SNL dans la présentation 3_4 "Séquenceur : SNL, Python et autres langages".

### IOC Shell

Une fois votre programme IOC démarré, vous atterrirez dans un Shell EPICS appelé l'IOC Shell. Ce
Shell dispose de ses propres commandes, consultable avec `help` :
```console
epics> help # check all available commands
epics> help dbl # check details of a specific command

epics> dbl
epics> dbpr
epics> dbpf

epics> epicsEnvShow
epics> epicsParamShow
epics> epicsThreadSleep 42
```

